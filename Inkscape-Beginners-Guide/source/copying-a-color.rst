***************
Copying a color
***************

There are several ways to apply the same style to multiple objects:

-  **Selecting multiple objects**: select all the objects whose color
   you want to change (hold :kbd:`Shift` and click, or drag a selection
   box around them), then open the :guilabel:`Fill and Stroke` dialog, select a color. It will be applied to all selected objects automatically.
-  **Use the Dropper tool**: select an object, then activate the Dropper
   tool by clicking on its icon |Dropper tool icon|, then click on the canvas, on an area where it has the color that you want to apply. The selected object's color will change accordingly.
-  **Copy the complete style of an object to apply it to
   another object:** select the object, copy it to the clipboard with :kbd:`Ctrl`
   + :kbd:`C`, then select the second object and paste only the style with
   :kbd:`Ctrl` + :kbd:`Shift` + :kbd:`V`. This not only copies the fill and
   stroke color, but also the stroke width and style, a text's font style and any
   patterns or gradients.



.. |Dropper tool icon| image:: images/icons/color-picker.*
   :class: inline
